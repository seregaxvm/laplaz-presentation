LATEX?=xelatex
TEXFLAGS?=-halt-on-error -file-line-error
PANDOCFLAGS=-s --mathjax --pdf-engine=lualatex
LATEXINDENT=latexindent
LATEXINDENT_SETTIGNS=indent.yaml
SRC_LANG?=ru

SRC:=$(wildcard *.tex)

pdf:
	latexmk -pdf -pdflatex="$(LATEX) $(TEXFLAGS)"

html: $(SRC)
	pandoc $(PANDOCFLAGS) --from latex --to html5 -o $(patsubst %.tex, %.html, $^) $^

doc: $(SRC)
	pandoc $(PANDOCFLAGS) --from latex --to odt -o $(patsubst %.tex, %.odt, $^) $^
	pandoc $(PANDOCFLAGS) --from latex --to docx -o $(patsubst %.tex, %.docx, $^) $^
clear:
	rm -f *.aux *.log *.out *.xml *.blg *.bcf *.bbl *.toc *.bak* *~ *.snm *.nav *.fls *.pdfpc \
*.vrb *.fdb_latexmk

clearall: clear
	rm -f *.pdf *.html *.odt *.doc *.docx

indent:
	$(LATEXINDENT) -l=$(LATEXINDENT_SETTIGNS) -s -w $(SRC)

spellcheck:
	aspell -l $(SRC_LANG) -c $(SRC)

.PHONY: pdf html doc clear clearall indent spellcheck
